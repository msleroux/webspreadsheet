var _letters = [];

var _rows = [];

const _functions = {
    Sum: "SUM",
    Count: "COUNT",
    Average: "AVG"
}

const _operators = {
    Add: "+",
    Subtract: "-",
    Multiply: "*",
    Divide: "/"
}

var _currentCell = null;

//TODO: Maybe read these values from UI
var _rowCount = 100;
var _columnCount = 100;

buildSheet();

function buildSheet() {
    generateHeader();
    generateCellRows();
}

function generateHeader() {
    var alphabet = [];
    var letterCount = 26;

    //Generate uppercase alphabet
    var code = "A".charCodeAt();
    for (var i = 0; i < letterCount; i++) {
        alphabet.push(String.fromCharCode(code));
        code++;
    }

    var rowElement = document.getElementById("sheetTopRow");

    var counter = 0;
    var i = 0;
    var prefixIndex = -1;
    var prefix = "";
    while (counter < _columnCount) {
        var letter = prefix + alphabet[i];
        _letters.push(letter);
        addElement(rowElement, "th", letter);
        i++;
        if (i == letterCount) {
            //Reached end of main letters, use prefix 
            i = 0;
            prefixIndex++;
            prefix = alphabet[prefixIndex];
        }
        counter++;
    }
}

function addElement(parent, tag, text) {
    var element = document.createElement(tag);
    if (text != "") {
        element.innerText = text;
    }
    parent.appendChild(element);
    return element;
}

function generateCellRows() {
    var parent = document.getElementById("sheetBody");

    var rowIndex = 0;
    for(rowIndex = 0; rowIndex < _rowCount; rowIndex++) {
        var rowElement = addElement(parent, "tr", "");
        var numberCell = addElement(rowElement, "td", rowIndex + 1);
        numberCell.setAttribute("class", "rowNumber");
    
        var row = {
            cells: []
        };

        row.cells = []; 
        var cellIndex = 0;
        for (cellIndex = 0; cellIndex < _columnCount; cellIndex++) {
            row.cells.push(generateCell(rowElement, rowIndex, cellIndex));
        }

        _rows.push(row);
    }
}

function generateCell(rowElement, rowIndex, cellIndex) {
    var cellElement = addElement(rowElement, "td", "");
    var input = addElement(cellElement, "input", "");
    input.type = "text";
    var functionCall = "cellClick(" + rowIndex + "," + cellIndex + ")";
    input.setAttribute("onclick", functionCall);
    input.setAttribute("onblur", "leaveCell()");
    input.setAttribute("class", "cellBox");
    var cellId = buildCellId(rowIndex, cellIndex);
    input.setAttribute("id", cellId);

    var cell = {
        id: cellId,
        value: "",
        displayValue: "",
        function: "",
        formulaOperators: [],
        formulaCellIds: []
    };

    return cell;
}

function buildCellId(rowIndex, cellIndex) {
    var rowNumber = rowIndex + 1;
    return _letters[cellIndex] + rowNumber;
}

function cellClick(rowIndex, cellIndex) {
    _currentCell = getCell(rowIndex, cellIndex);
    var input = getCurrentCellInput();
    input.value = _currentCell.value; //ensure user can edit formula
}

function getCell(rowIndex, cellIndex) {
    var row = _rows[rowIndex];
    return row.cells[cellIndex];
}

function getCurrentCellInput() {
    return document.getElementById(_currentCell.id);
}

function leaveCell() {
    if (_currentCell != null) {
        var input = getCurrentCellInput();
        _currentCell.value = input.value;

        var result = checkForSpecialInput();

        if (result === "") {
            //Not a function or formula
            _currentCell.displayValue = _currentCell.value;
            _rows.forEach(checkIfCellUsedInCalculations);
        } else {
            //Show calculation result
            _currentCell.displayValue = result; 
            input.value = result;
        }
    }
}

function checkForSpecialInput() {
    var result = "";

    var trimmedValue = _currentCell.value.trim(); //if this is a special value, remove leading spaces to simplify initial check
    if (trimmedValue.startsWith("="))
    {
        //Remove all remaining spaces
        var index = 0;
        while (index >= 0) {
            trimmedValue = trimmedValue.replace(" ", "");
            index = trimmedValue.indexOf(" ");
        }

        _currentCell.value = trimmedValue;
        //Remove leading sign and convert to uppercase to simplify remaining processing
        trimmedValue = trimmedValue.substr(1).toUpperCase(); 
        result = checkForFunction(trimmedValue);
        if (result === "") {
            result = checkForFormula(trimmedValue);
        }
    }

    return result;
}

function checkForFunction(cellValue) {
    //Checks if user's input is a predefined function 
    var result = "";
    _currentCell.function = "";
    var func = "";
    var funcNames = [_functions.Sum, _functions.Count, _functions.Average];

    var i = 0;
    //Check if cell value starts with a predefined function
    for (i = 0; i < funcNames.length; i++) {
        if (cellValue.startsWith(funcNames[i])) {
            func = funcNames[i]; 
            break;
        }    
    }

    if (func !== "") {
        //Remove function name to process parameters
        cellValue = cellValue.substr(func.length);
        _currentCell.function = func;
        var checkResult = validateFunction(cellValue);

        if (checkResult.isValid) {
            //For now, assume that validation has checked for all potential invalid characters
            //and that remaining content is only valid cell positions and a colon
            setFunctionRange(checkResult.cellValue);
            result = calculateFunctionResult(func, _currentCell.formulaCellIds);
        }
        else {
            console.warn("Function invalid: " + checkResult.failReason);
        }
    }

    return result;
}

function setFunctionRange(rangeValue) {
    var positions = rangeValue.split(":");

    //Extract cell locations
    var rangeStart = getCellLocation(positions[0]);

    var range = {
        startRowIndex: rangeStart.rowIndex,
        startCellIndex: rangeStart.cellIndex,
        endRowIndex: rangeStart.rowIndex, //default to start in case user didn't specify second value
        endCellIndex: rangeStart.cellIndex
    };

    if (positions.length > 1) {
        var rangeEnd = getCellLocation(positions[1]);
        //If user put lower row and/or column as range end, switch start and end range values
        //to ensure that range start is always lower than range end
        if (rangeEnd.rowIndex >= range.startRowIndex) {
            range.endRowIndex = rangeEnd.rowIndex;
        } else {
            range.endRowIndex = range.startRowIndex;
            range.startRowIndex = rangeEnd.rowIndex;
        }
        if (rangeEnd.cellIndex >= range.startCellIndex) {
            range.endCellIndex = rangeEnd.cellIndex;
        } else {
            range.endCellIndex = range.startCellIndex;
            range.startCellIndex = rangeEnd.cellIndex;
        }
    }

    _currentCell.formulaCellIds = [];
    var rowIndex = 0;
    for (rowIndex = range.startRowIndex; rowIndex <= range.endRowIndex; rowIndex++) {
        var cellIndex = 0;
        for (cellIndex = range.startCellIndex; cellIndex <= range.endCellIndex; cellIndex++) {
            _currentCell.formulaCellIds.push(buildCellId(rowIndex, cellIndex));
        }
    }
}

function calculateFunctionResult(func, formulaCellIds) {
    var numbers = getFormulaCellNumbers(formulaCellIds);

    if (numbers.length > 0) {
        switch (func) {
            case _functions.Sum:
                return calculateSum(numbers);
            case _functions.Count:
                return numbers.length;
            case _functions.Average:
                var sum = calculateSum(numbers);
                var avg = sum / numbers.length;
                return "" + avg;        
                //TODO: remaining functions
        }
    
        return "";    
    }
    return "-"; //default if no numbers in range
}

function hasElements(array) {
    return array != null && array != undefined && array.length > 0;
}

function getFormulaCellNumbers(formulaCellIds) {
    var numbers = [];

    if (hasElements(formulaCellIds)) {
        var i = 0;
        for (i = 0; i < formulaCellIds.length; i++) {
            var value = getInputNumber(formulaCellIds[i]);
            if (value !== "") {
                numbers.push(parseInt(value, 10));
            }
        }
    }

    return numbers;
}

function getInputNumber(cellId) {
    var input = document.getElementById(cellId);
    if (input != null && input != undefined && input.value.trim() !== "" && !isNaN(input.value)) {
        return input.value;
    }
    return "";
}

function calculateSum(numbers) {
    if (hasElements(numbers)) {
        var sum = 0;
        var i = 0;
        for (i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }

        return sum;
    }

    return "-"; //default if no values found for calculation
}

function getCellLocation(position) {
    //position will be a cell location, e.g. "A1"
    var codeA = "A".charCodeAt();
    var codeZ = "Z".charCodeAt();

    //TODO: Validation to ensure that position is letter-number combination

    var i = 0;
    var column = "";
    var rowNumber = "";
    //Extract letters for column position and numbers for row number
    for (i = 0; i < position.length; i++) {
        var character = position.substr(i, 1);
        var code = character.charCodeAt();
        if (code >= codeA && code <= codeZ) {
            //Busy with column character(s)
            column += character;
        } else if (!isNaN(character)) {
            rowNumber += character;
        }
    }

    var result = {
        rowIndex: rowNumber - 1,
        cellIndex: _letters.indexOf(column)
    };

    //Ensure cells cannot exceed current range
    //TODO: Make this part of validation
    if (result.rowIndex >= _rowCount) {
        result.rowIndex = _rowCount - 1;
    }

    if (result.cellIndex >= _letters.length) {
        result.cellIndex = _letters.length - 1;
    }

    return result;
}

function validateFunction(cellValue) {
    //Check if remaining value is surrounding by parameters
    var isValid = true;
    var failReason = "";
    if (cellValue.startsWith("(")) {
        //Remove leading parenthesis
        cellValue = cellValue.substr(1);
    }
    else {
        isValid = false;
        failReason = "No opening parenthesis."
    } 
    if (cellValue.slice(-1) === ")") {
        //Remove closing parenthesis
        cellValue = cellValue.substr(0, cellValue.length - 1);
    } else {
        isValid = false;
        failReason = appendString(failReason, "No closing parenthesis.");
    }

    //TODO: Additional checks for invalid characters

    if (cellValue === "") {
        isValid = false;
        failReason = appendString(failReason, "No function values."); //TODO: clearer message
    }

    var result = {
        cellValue: cellValue,
        isValid: isValid,
        failReason: failReason
    };

    return result;
}

function appendString(main, toAppend) {
    if (main !== "") {
        main += " ";
    }
    return main += toAppend;
}

function checkForFormula(cellValue) {
    //Checks if the user's input is a formula, e.g. "A1+A2"
    var result = "-";

    if (checkFormula(cellValue)) {
        if (hasElements(_currentCell.formulaCellIds) && hasElements(_currentCell.formulaOperators)) {
            result = calculateFormulaResult(_currentCell.formulaCellIds, _currentCell.formulaOperators);
        }
    }

    return result;
}

function checkFormula(cellValue) {
    var isValid = true;

    var codeA = "A".charCodeAt();
    var codeZ = "Z".charCodeAt();

    var operators = [_operators.Add, _operators.Subtract, _operators.Multiply, _operators.Divide];

    _currentCell.formulaCellIds = [];
    _currentCell.formulaOperators = [];

    var failReason = "";

    //Check character by character - letter/number combination regarded as cell, anything else regarded as operator
    //TODO: More validation e.g. number before letter
    var cellId = "";
    var i = 0;
    for (i = 0; i < cellValue.length; i++) {
        var character = cellValue.substr(i, 1);
        var code = character.charCodeAt();
        if ((code >= codeA && code <= codeZ) || !isNaN(character)) {
            //Busy with letter or number
            cellId += character;
        }
        else {
            //Not letter or number, check if this is a valid operator
            if (operators.indexOf(character) >= 0) {
                //Character is in list of acceptable operators
                if (cellId != "") {
                    //Character is preceded by a cell ID
                    _currentCell.formulaCellIds.push(cellId);
                    _currentCell.formulaOperators.push(character);
                    cellId = "";
                } else {
                    isValid = false;
                    if (i == cellValue.length - 1) {
                        //End of input
                        appendString(failReason, "Formula cannot end on an operator.");
                    } else {
                        appendString(failReason, "Operator must be preceded by a cell identifier.");
                    }
                }
            } else {
                isValid = false;
                appendString(failReason, "'" + character + "' is not a valid operator.");
            }
        } 
    }

    if (cellId !== "") {
        //input ends with a cell ID
        _currentCell.formulaCellIds.push(cellId);
    }

    if (!isValid) {
        console.warn(failReason);
    }

    return isValid;
}

function calculateFormulaResult(formulaCellIds, formulaOperators) {
    var result = "-";

    var i = 0;

    var total = 0;
    var foundNumber = false;
    //TODO: Implement proper math, i.e. multiply/divide, then add/subtract
    
    for (i = 0; i < formulaCellIds.length; i++)
    {
        var value = getInputNumber(formulaCellIds[i]);

        if (value !== "") {
            var number = parseInt(value, 10);
            if (foundNumber) {
                if (i <= formulaOperators.length) {
                    var operator = formulaOperators[i - 1];
                    total = processFormulaNumber(total, number, operator);
                }
                else {
                    break; //no more operators
                }
            }
            else {
                //Assign first actual number to the total, then adjust total further as other numbers found
                total = number;
                foundNumber = true;
            }
        }
    }

    if (foundNumber) {
        result = "" + total;
    }

    return result;
}

function processFormulaNumber(currentTotal, number, operator) {
    switch (operator) {
        case _operators.Add:
            currentTotal += number;
            break;
        case _operators.Subtract:
            currentTotal -= number;
            break;
        case _operators.Multiply:
            currentTotal *= number;
            break;
        case _operators.Divide:
            //TODO: How to handle zero?
            if (number !== 0) {
                currentTotal = currentTotal / number;
            }
            break;
    }

    return currentTotal;
}

function checkIfCellUsedInCalculations(row, index, array) {
    var c = 0;
    for (c = 0; c < row.cells.length; c++) {
        var cell = row.cells[c];

        if (_currentCell.id != cell.id && hasElements(cell.formulaCellIds)) {
            //Not the cell the user just finished with, and it contains a formula or function
            if (cell.formulaCellIds.indexOf(_currentCell.id) >= 0) {
                //Current cell is used in this cell's calculation
                if (cell.function !== "") {
                    cell.displayValue = calculateFunctionResult(cell.function, cell.formulaCellIds);
                }
                else if (hasElements(cell.formulaOperators)) {
                    cell.displayValue = calculateFormulaResult(cell.formulaCellIds, cell.formulaOperators);
                }
                document.getElementById(cell.id).value = cell.displayValue;
            }
        }
    }
}

function refreshSheet() {
    var now = new Date(); 
    var date = now.getDate() + "/"
        + (now.getMonth()+1)  + "/" 
        + now.getFullYear() + " @ "  
        + now.getHours() + ":"  
        + now.getMinutes() + ":" 
        + now.getSeconds();

    //Show the date of the last refresh to confirm that pzge was refreshed
    document.getElementById("lastRefresh").innerText = "Last refresh: " + date;

    _rows.forEach(refreshRow);
}

function refreshRow(row, index, array) {
    var c = 0;
    for (c = 0; c < row.cells.length; c++) {
        var cell = row.cells[c];
        var input = document.getElementById(cell.id);
        input.value = cell.displayValue;
    }
}
